function preload() {
  proc = loadImage("imgs/processore.png");
  tri = loadImage("imgs/tri.png")
  font = loadFont("fonts/Fiendish_Regular.ttf")

  prev = -1
  gifFlag = true
}

function setup() {
  createCanvas(800, 1000);
  background("#00923fff");

  const frmRate = 60
  frameRate(frmRate)

  k = 20;

  rangex = (width / k);
  rangey = (height / k) * 1.2;

  xs = [];
  ys = [];
  isCircuit = [];
  circuitDirection = [];
  eyeDir = []

  for (let x = -rangex / 2; x < rangex / 2; x++) {
    for (let y = -rangey / 2; y < rangey / 2; y++) {
      no = spiralNrNoMax(x, y);

      if (primes.includes(no + 1)) {
        let index = primes.indexOf(no + 1);
        xs[index] = x;
        ys[index] = y;
        isCircuit[index] = random();
        circuitDirection[index] = random();
        eyeDir[index] = random();
      }
    }
  }

  eyeHor = createEyeHor(k)
  eyeVer = createEyeVer(k)
  writing = createText(color("#f71500ff"))
  echo = createText(color(255,150))

  defaultSpeed = 1
  slider = createSlider(-20, 20, defaultSpeed,1);
  slider.position(10, 10);
  slider.style('width', '200px');

  //flag = false

  counter = 0;
}

function draw() {
  translate(width / 2, 400);
  scale(1, -1);

  let divisor = 1000;
  let offset = -1;
  let osc = sin(counter / divisor + offset);

  let scale_factor = map(osc, 0, 1, k/5, 1);
  scale(scale_factor, scale_factor);

  //Colore Sfondo
  let pcb=color("#00923fff")
  let blood=color("#600000")
  let backOsc=map(osc,0.8,1,0,1)
  let bg=lerpColor(pcb,blood,backOsc)
  background(bg);

  //Colore Linee
  let outline=color(231,179,23,255)
  stroke(outline);
  let weightOsc=max(map(osc,0.8,-1,0,1.5),0)
  strokeWeight(weightOsc)

  for (i = 0; i < xs.length; i++) {
    let x = xs[i] * k;
    let y = ys[i] * k;
    1
    if (isCircuit[i] < 0.05) {
      if (circuitDirection[i] > 0.5) {
        line(0, 0, x, 0);
        line(x, 0, x, y);
      } else {
        line(0, 0, 0, y);
        line(0, y, x, y);
      }
    }
  }
  
  //Colore Punti
  stroke(outline);
  let dotFirst=color("#e7c96fff")
  let dotSecond=color("#e4143c")
  let dotColor=lerpColor(dotFirst,dotSecond,backOsc)
  fill(dotColor);
  kadj = k - (osc+1)
  
  for (i = 0; i < xs.length; i++) {
    let x = xs[i] * k;
    let y = ys[i] * k;

    
    circle(x, y, k-weightOsc);
  }

  //Processore
  imageMode(CENTER)
  let sizeProc=k*4
  let tintOsc=map(osc,1,0,0,255)
  tint(255,tintOsc)
  image(proc,0,0,sizeProc,sizeProc)




  irisOsc=map(osc,1,0.8,255,0)

  tint(255,irisOsc)

  for (i = 0; i < xs.length; i++) {
    let x = xs[i] * k;
    let y = ys[i] * k;

    if (eyeDir[i]<0.5){
    image(eyeHor,x,y, k,k)
    } else {
      image(eyeVer,x,y, k,k)
    }
  }

  //Iridi
  noStroke()
  fill(60,0,0,irisOsc)
  for (i = 0; i < xs.length; i++) {
    let x = xs[i] * k;
    let y = ys[i] * k;


    
    circle(x+noise(x+counter/20)*10-5, y+noise(y+counter/20)*10-5, k*0.3);
  }


  push()
  scale(1,-1)
  tint(255,irisOsc)
  image(tri,0,0,960*.3,960*.3)
  pop()

  push()
  fill(0,0,0,255)
  tint(255,irisOsc)
  scale(1/scale_factor, -1/scale_factor);
  rect(-width/2, 400,width,200)
  //fill(255)
  //text(str(counter),0,500)
  textOffset = 15
  shuffleX = noise(counter)*textOffset - textOffset/2
  shuffleY = noise(-counter)*textOffset - textOffset/2
  image(writing,0,500)
  image(echo,shuffleX,500+shuffleY)
  pop()


  /*
  if (osc >= 0.99 && !flag)  {
    saveCanvas("story")
    flag = true
  }
  */


  if (counter==0){
  saveGif('01.gif', 96);
  }

  if (cos(counter / divisor + offset) <0){
  counter += slider.value()*5;
  } else {
    counter += slider.value();
  }


}

function spiralNrNoMax(x, y) {
  if (y < x && -x < y) return -3 * x + y + 4 * x * x;
  if (x <= y && -x < y) return -x - y + 4 * y * y;
  if (x <= y && y <= -x) return -x - y + 4 * x * x;
  if (y < x && y <= -x) return x - 3 * y + 4 * y * y;
}

function createEyeHor(ksmall) {
  let k=ksmall*2
  let denominator = 4
  let cnv1 = createGraphics(k,k);
  cnv1.strokeWeight(0)
  cnv1.fill(0,0);
  cnv1.circle(k/denominator,k/2,k);
  let ctx1 = cnv1.canvas.getContext("2d");
  ctx1.clip();
  cnv1.fill("#ffffff")
  cnv1.circle(k*(denominator-1)/denominator,k/2,k)
  return cnv1
}

function createEyeVer(ksmall) {
  let k=ksmall*2
  let denominator = 4
  let cnv1 = createGraphics(k,k);
  cnv1.strokeWeight(0)
  cnv1.fill(0,0);
  cnv1.circle(k/2,k/denominator,k);
  let ctx1 = cnv1.canvas.getContext("2d");
  ctx1.clip();
  cnv1.fill("#ffffff")
  cnv1.circle(k/2,k*(denominator-1)/denominator,k)
  return cnv1
}

function createText(col){
  let cnv1 = createGraphics(800,200);
  cnv1.translate(cnv1.width / 2, cnv1.height/2);
  cnv1.textFont(font)
  cnv1.textSize(25)
  cnv1.textLeading(70);
  cnv1.fill(col)
  cnv1.textAlign(CENTER,CENTER)
  cnv1.text("Patetica Creatura di Carne e Compositi,\ncome puoi sfidare un essere come me!",0,0)
  return cnv1
}